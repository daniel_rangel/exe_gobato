# primeiro commit


-   Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato
$ git init
Initialized empty Git repository in C:/Users/Samsung/Desktop/AulaGobato/.git/

-   Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        index.html
        scripty.js
        server.js

nothing added to commit but untracked files present (use "git add" to track)

-   Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git add .

-   Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git commit -m "primeiro commit"
[master (root-commit) d34367d] primeiro commit
 3 files changed, 12 insertions(+)
 create mode 100644 index.html
 create mode 100644 scripty.js
 create mode 100644 server.js
-   Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   scripty.js

no changes added to commit (use "git add" and/or "git commit -a")

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git add .

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git commit -m "segundo commit"
[master 8644ac7] segundo commit
 1 file changed, 3 insertions(+)

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git remote add origin https://gitlab.com/daniel_rangel/exe_gobato.git

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git remote -v
origin  https://gitlab.com/daniel_rangel/exe_gobato.git (fetch)
origin  https://gitlab.com/daniel_rangel/exe_gobato.git (push)

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/AulaGobato (master)
$ git push -u origin master
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 4 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 997 bytes | 332.00 KiB/s, done.
Total 10 (delta 2), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/daniel_rangel/exe_gobato.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.

/* glone */


- Samsung@Daniel-Rangel MINGW64 ~/Desktop/Nova pasta
$ git clone https://gitlab.com/alexandergobbato/tpsmp.git
Cloning into 'tpsmp'...
remote: Enumerating objects: 28, done.
remote: Counting objects: 100% (28/28), done.
remote: Compressing objects: 100% (17/17), done.
remote: Total 28 (delta 9), reused 27 (delta 9), pack-reused 0
Unpacking objects: 100% (28/28), 2.89 KiB | 22.00 KiB/s, done.

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/Nova pasta
$ cd tpsmp

- Samsung@Daniel-Rangel MINGW64 ~/Desktop/Nova pasta/tpsmp (master)
$ git show

```
commit 52fa531c1ef76ff82e9054a8d560ee87146936d6 (HEAD -> master, tag: v1.0.0, origin/master, origin/feature_funcao, origin/HEAD)
Author: Alexander Gobbato <alexandergobbato@gmail.com>
Date:   Wed Mar 10 21:34:10 2021 -0300

    Inserindo exemplos de função

diff --git a/exJS/intro.js b/exJS/intro.js
index 762adc9..115610b 100644
--- a/exJS/intro.js
+++ b/exJS/intro.js
@@ -21,3 +21,31 @@ try{
 }catch(error){
     console.log("Erro na variável idade!");
 }
+
+function mostrarIdade(){
+    let idade = 42;
+    if(idade < 42){
+        console.log("Jovem");
+    }else{
+        console.log("Não muito Jovem");
+    }
+}
+mostrarIdade();
+
+function mostrarIdade2(vlIdade){
+    if(vlIdade < 42){
+        console.log(vlIdade + " - Jovem");
+    }else{
+        console.log(vlIdade + " - Não muito Jovem");
+    }
+}
+mostrarIdade2(42);
+
+const mostraIdade3 = (vlIdade) => {
+    if(vlIdade < 42){
+        console.log(vlIdade + " - Jovem - versão 3");
+    }else{
+        console.log(vlIdade + " - Não muito Jovem - versão 3");
+    }
+}
+mostraIdade3(42);
(END)
```

